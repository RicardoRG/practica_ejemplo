<?php

/* demoDemoBundle:Default:index.html.twig */
class __TwigTemplate_8525a77abeb66e04ae6c3441a75c1530b72e5cd6e046c0dbbdc793fb4fdd4777 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<html>
    <head><title>Practica</title></head>
    <body>
        <form method=\"Post\" role=\"form\"  align=\"middle\" id=\"formp\">
            <br>
            <br>
            Escribe tu nombre: 
            <input type=\"text\" name=\"nom\" id=\"nombre\"/>
            <br>
            <br>
            Escribe tu apellido:
            <input type=\"text\" name=\"ape\" id=\"apellido\"/>
            <br>
            <br>
            <input id=\"btn_insertar\" type=\"submit\" value=\"Insertar\"/>
            <input id=\"btn_modificar\" type=\"submit\" value=\"Modificar\"/>
            <input id=\"btn_eliminar\"  type=\"submit\" value=\"Borrar\"/>
            <input id=\"btn_buscar\"  type=\"submit\" value=\"Buscar\"/>
        </form>
        <div id = \"resultado\">
        </div>
    </body>

    <script src=\"";
        // line 25
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("public/js/jquery-1.11.3.min.js"), "html", null, true);
        echo "\"></script>

    <script type=\"text/javascript\">
                \$(document).ready(function () {
        var \$variable = null;
                \$('#btn_insertar').click(function () {
        var \$nombre = \$('#nombre').val();
                var \$apellido = \$('#apellido').val();
                var data = [\$nombre, \$apellido];
                \$variable = 'insertar';
                console.log(data);
                var URL = \"";
        // line 36
        echo $this->env->getExtension('routing')->getPath("demo_ajax");
        echo "\";
                console.log(URL);
                \$.ajax({
                type: \"POST\",
                        url: URL,
                        data:{ data, tip: \$variable },
                        success: function (data)
                        {
                        \$(\"#resultado\").html(data);
                        }
                });
                return false;
        });
                \$('#btn_buscar').click(function () {
        var \$nombre = \$('#nombre').val();
                var \$apellido = \$('#apellido').val();
                var data = [\$nombre, \$apellido];
                \$variable = 'buscar';
                console.log(data);
                var URL = \"";
        // line 55
        echo $this->env->getExtension('routing')->getPath("demo_ajax");
        echo "\";
                
                \$.ajax({
                type: \"POST\",
                        url: URL,
                        data:{ data, tip: \$variable },
                        success: function (data)
                        {
                        \$(\"#resultado\").html(data);
                        }
                });
                return false;
        });
        });

    </script>

</html>";
    }

    public function getTemplateName()
    {
        return "demoDemoBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  81 => 55,  59 => 36,  45 => 25,  19 => 1,);
    }
}
