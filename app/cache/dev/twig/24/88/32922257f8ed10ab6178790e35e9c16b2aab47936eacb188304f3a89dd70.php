<?php

/* demoDemoBundle:Default:result.html.twig */
class __TwigTemplate_248832922257f8ed10ab6178790e35e9c16b2aab47936eacb188304f3a89dd70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        <title>title</title>
    </head>
    <body>
        <div>
            ";
        // line 7
        if (array_key_exists("arreglo", $context)) {
            // line 8
            echo "                <table width='70%' border='.7' align='center'>
                    <thead>
                        <tr>
                            <th>
                                id
                            </th>
                            <th>
                                Nombre
                            </th>
                            <th>
                                Apellido
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                            <tr>
                                <td align='center'>
                                    ";
            // line 25
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["arreglo"]) ? $context["arreglo"] : $this->getContext($context, "arreglo")), "id_usr", array(), "array"), "html", null, true);
            echo "
                                </td>
                                <td align='center'>
                                    ";
            // line 28
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["arreglo"]) ? $context["arreglo"] : $this->getContext($context, "arreglo")), "Nombre", array(), "array"), "html", null, true);
            echo "
                                </td>
                                <td align='center'>
                                    ";
            // line 31
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["arreglo"]) ? $context["arreglo"] : $this->getContext($context, "arreglo")), "Apellido", array(), "array"), "html", null, true);
            echo "
                                </td>
                            </tr>
                    </tbody>
                </table>
            ";
        }
        // line 37
        echo "        </div>
    </body>
</html>

";
    }

    public function getTemplateName()
    {
        return "demoDemoBundle:Default:result.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 37,  60 => 31,  54 => 28,  48 => 25,  29 => 8,  27 => 7,  19 => 1,);
    }
}
