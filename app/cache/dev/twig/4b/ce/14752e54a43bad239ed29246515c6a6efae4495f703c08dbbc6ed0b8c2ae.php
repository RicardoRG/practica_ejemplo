<?php

/* AdminloginBundle:Default:registrojs.html.twig */
class __TwigTemplate_4bce14752e54a43bad239ed29246515c6a6efae4495f703c08dbbc6ed0b8c2ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <head>
        <link href=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("public/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <title>Registro</title>
    <head>
    <body>
        <div class=\"modal fade in\" >
            <div class=\"modal-header\">
                <h4 class=\"model-title\" align=\"center\"> Registrarse </h4>
            </div>
            <div class=\"modal-body\">
                <div id=\"div_registro\" >
                </div>
            </div>
            <div class=\"modal-footer\" align=\"center\" id=\"div_btn\">
            </div>
        </div>

    </body>
    <script src=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("public/js/jquery-1.11.3.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("public/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        var divfrm = document.getElementById(\"div_registro\");
        var divbtn = document.getElementById(\"div_btn\");


        var form = document.createElement(\"form\");
        form.id = \"frm_registro\";
        form.name = \"login\";
        form.method = \"post\";
        divfrm.align = \"center\";
        form.className = \"form-horizontal\";
        divfrm.appendChild(form);

        var txt_Nombre = document.createElement(\"input\");
        txt_Nombre.type = 'text';
        txt_Nombre.id = 'txt_Nombre';
        txt_Nombre.name = 'Nombre';
        txt_Nombre.required;
        txt_Nombre.placeholder = \"Introduce tu Nombre \";
        txt_Nombre.className = 'form-control input-lg';
        lbl = document.createElement('label');
        lbl.innerHTML = \"Nombre: \";
        lbl.style.display = \"block\";
        form.appendChild(lbl);
        form.appendChild(txt_Nombre);

        var txt_ApeP = document.createElement(\"input\");
        txt_ApeP.type = 'text';
        txt_ApeP.id = 'txt_ApellidoP';
        txt_ApeP.name = 'ApellidoPaterno';
        txt_ApeP.required;
        txt_ApeP.placeholder = \"Introduce tu Apellido Paterno \";
        txt_ApeP.className = 'form-control input-lg';
        lbl = document.createElement('label');
        lbl.innerHTML = \"Apellido Paterno: \";
        lbl.style.display = \"block\";
        form.appendChild(lbl);
        form.appendChild(txt_ApeP);

        var txt_ApeM = document.createElement(\"input\");
        txt_ApeM.type = 'text';
        txt_ApeM.id = 'txt_ApellidoM';
        txt_ApeM.name = 'ApellidoMaterno';
        txt_ApeM.required;
        txt_ApeM.placeholder = \"Introduce tu Apellido Materno \";
        txt_ApeM.className = 'form-control input-lg';
        lbl = document.createElement('label');
        lbl.innerHTML = \"Apellido Materno: \";
        lbl.style.display = \"block\";
        form.appendChild(lbl);
        form.appendChild(txt_ApeM);

        var txt_Email = document.createElement(\"input\");
        txt_Email.type = 'text';
        txt_Email.id = 'txt_Email';
        txt_Email.name = 'Email';
        txt_Email.required;
        txt_Email.placeholder = \" Email: visitante@gmail.com.mx \";
        txt_Email.className = 'form-control input-lg';
        lbl = document.createElement('label');
        lbl.innerHTML = \"Email: \";
        lbl.style.display = \"block\";
        form.appendChild(lbl);
        form.appendChild(txt_Email);

        var txt_Password = document.createElement(\"input\");
        txt_Password.type = 'text';
        txt_Password.id = 'txt_Password';
        txt_Password.name = 'Password';
        txt_Password.required;
        txt_Password.placeholder = \" Introduce tu Password \";
        txt_Password.className = 'form-control input-lg';
        lbl = document.createElement('label');
        lbl.innerHTML = \"Password: \";
        lbl.style.display = \"block\";
        form.appendChild(lbl);
        form.appendChild(txt_Password);

        var txt_Cargo = document.createElement(\"input\");
        txt_Cargo.type = 'text';
        txt_Cargo.id = 'txt_Cargo';
        txt_Cargo.name = 'DE_Cargo';
        txt_Cargo.required;
        txt_Cargo.placeholder = \" Introduce tu Cargo \";
        txt_Cargo.className = 'form-control input-lg';
        lbl = document.createElement('label');
        lbl.innerHTML = \"Cargo: \";
        lbl.style.display = \"block\";
        form.appendChild(lbl);
        form.appendChild(txt_Cargo);

        var txt_Razon = document.createElement(\"input\");
        txt_Razon.type = 'text';
        txt_Razon.id = 'txt_Razon';
        txt_Razon.name = 'DE_Razon_Social';
        txt_Razon.required;
        txt_Razon.placeholder = \" Introduce la Razón Social \";
        txt_Razon.className = 'form-control input-lg';
        lbl = document.createElement('label');
        lbl.innerHTML = \"Razón Social: \";
        lbl.style.display = \"block\";
        form.appendChild(lbl);
        form.appendChild(txt_Razon);

        var btn_Regis = document.createElement(\"input\");
        btn_Regis.type = 'button';
        btn_Regis.id = 'btn_registrar';
        btn_Regis.name = 'Registrar';
        btn_Regis.value = 'Registrar';
        divbtn.appendChild(btn_Regis);
        btn_Regis.className = 'btn btn-primary btn-lg';


        var btn_Cancelar = document.createElement(\"input\");
        btn_Cancelar.type = 'button';
        btn_Cancelar.id = 'btn_Cancelar';
        btn_Cancelar.name = 'Cancelar';
        btn_Cancelar.value = 'Cancelar';
        divbtn.appendChild(btn_Cancelar);
        btn_Cancelar.className = 'btn btn-primary btn-lg';

        \$('#btn_registrar').click(function () {
            var data = \$(\"#frm_registro\").serialize();
            var URL = \"";
        // line 145
        echo $this->env->getExtension('routing')->getPath("registrar");
        echo "\";
            console.log(data);
            \$.ajax({
                type: \"POST\",
                url: URL,
                data: {data}
            });
            alert(\"Registro Exitoso\");
            \$.get(\"";
        // line 153
        echo $this->env->getExtension('routing')->getPath("login");
        echo "\", function (htmlexterno) {
                \$(\"#index\").html(htmlexterno);
            });
        });

        \$(\"#btn_Cancelar\").click(function () {
            \$.get(\"";
        // line 159
        echo $this->env->getExtension('routing')->getPath("login");
        echo "\", function (htmlexterno) {
                \$(\"#index\").html(htmlexterno);
            });
        });
    </script>

</html>";
    }

    public function getTemplateName()
    {
        return "AdminloginBundle:Default:registrojs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 159,  185 => 153,  174 => 145,  47 => 21,  43 => 20,  23 => 3,  19 => 1,);
    }
}
