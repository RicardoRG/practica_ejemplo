<?php

/* AdminloginBundle:Default:registro.html.twig */
class __TwigTemplate_7f76129e1814b203c69afedd404d0399525bccbae1629dea7a7e8987826b8c32 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <body>
<div align=\"center\" >
    <form method=\"post\" id=\"frm_registro\"  >
       ";
        // line 6
        echo "        <p>    Nombre  <input type=\"text\" name=\"Nombre\"  placeholder=\"Nombre Visitante\"></p>
        <p>Apellido P.  <input type=\"text\" name=\"ApellidoPaterno\"  placeholder=\"Apellido Paterno\"></p>
        <p>Apellido M.  <input type=\"text\" name=\"ApellidoMaterno\"  placeholder=\"Apellido Materno\"></p>
        <p>    E-mail  <input type=\"text\" name=\"Email\"  placeholder=\" Email Visitante\"></p>
        <p>  Password  <input type=\"password\" name=\"Password\" value=\"\" placeholder=\"Password\"></p>
        <p>     Cargo  <input type=\"text\" name=\"DE_Cargo\" value=\"\" placeholder=\"Cargo Visitante\"></p>
        <p>Razón Social<input type=\"text\" name=\"DE_Razon_Social\" value=\"\" placeholder=\"Razón Social\"></p>
        <input type=\"submit\" name=\"commit\" value=\"Registrar\" id=\"btn_registrar\">
        <input type=\"submit\" name=\"commit\" value=\"Cancelar\">
        
    </form>
</div>
<script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("public/js/jquery-1.11.3.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\">
    \$('#btn_registrar').click(function () {
        var data = \$(\"#frm_registro\").serialize();
        var URL = \"";
        // line 22
        echo $this->env->getExtension('routing')->getPath("registrar");
        echo "\";
        console.log(data);
        \$.ajax({
            type: \"POST\",
            url: URL,
            data: {data},
            success: function (data)
            {
                \$(\"#resultado\").html(data);
            }
        });
        return false;
    });
</script>
</html>";
    }

    public function getTemplateName()
    {
        return "AdminloginBundle:Default:registro.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 22,  39 => 18,  25 => 6,  19 => 1,);
    }
}
