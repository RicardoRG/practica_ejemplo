<?php

/* AdminloginBundle:Default:formjavascript.html.twig */
class __TwigTemplate_d846267305fb395b041b660d6053c5614dd5223ef3527426bb9c239fcb3113f1 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 4
        echo "
<html>

    <head>
        <link href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("public/css/bootstrap.css"), "html", null, true);
        echo "\" rel=\"stylesheet\" />
        <title>Login Visitante</title>
    </head>
    <body>
        <div class=\"modal fade in\" id=\"index\">
            <div class=\"modal-header\">
                <h4 class=\"model-title\" align=\"center\">Login</h4>
            </div>

            <div class=\"modal-body\">
                <div id=\"div_login\">  
                    
                </div> 
            </div>
            <div class=\"modal-footer\" align=\"center\" id=\"div_btn\">
            </div>
            
        </div>
         
    </body>
    <script src=\"";
        // line 28
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("public/js/jquery-1.11.3.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("public/js/bootstrap.js"), "html", null, true);
        echo "\"></script>
    <script type=\"text/javascript\">
        var divfrm = document.getElementById(\"div_login\");
        var divfoot = document.getElementById(\"div_btn\");


        var form = document.createElement(\"form\");
        divfrm.appendChild(form);
        form.id = \"frm_login\";
        form.name = \"login\";
        form.method = \"post\";
        divfrm.align = \"middle\";
        // form.className = \"form-group\";

        var txt_Email = document.createElement(\"input\");
        txt_Email.type = 'text';
        txt_Email.id = 'txt_Email';
        txt_Email.name = 'Email';
        txt_Email.className = 'form-control input-lg';
        txt_Email.placeholder = \"Introduce tu E-mail\";
        lbl = document.createElement('label');
        lbl.innerHTML = \"Email: \";
        form.appendChild(lbl);
        form.appendChild(txt_Email);
        txt_Email.style.display = \"block\";

        var txt_Pass = document.createElement(\"input\");
        txt_Pass.type = 'password';
        txt_Pass.id = 'txt_Pass';
        txt_Pass.name = 'Password';
        txt_Pass.placeholder = \"Introduce tu Password\";
        lbl = document.createElement('label');
        lbl.innerHTML = 'Password: ';
        form.appendChild(lbl);
        form.appendChild(txt_Pass);
        txt_Pass.style.display = \"block\";
        txt_Pass.className = 'form-control';

        var btn_login = document.createElement(\"input\");
        btn_login.type = 'button';
        btn_login.id = 'btn_login';
        btn_login.name = 'Login';
        btn_login.value = 'Login';
        divfoot.appendChild(btn_login);
        btn_login.className = 'btn btn-primary btn-lg';

        var btn_registro = document.createElement(\"input\");
        btn_registro.type = 'button';
        btn_registro.id = 'btn_registro';
        btn_registro.name = 'Registro';
        btn_registro.value = 'Registro';
        divfoot.appendChild(btn_registro);
        btn_registro.className = 'btn btn-primary btn-lg';
   


        \$('#btn_login').click(function () {
            var data = \$(\"#frm_login\").serialize();
            var URL = \"";
        // line 87
        echo $this->env->getExtension('routing')->getPath("consulta");
        echo "\";
            console.log(data);
            \$.ajax({
                type: \"POST\",
                url: URL,
                data: {data},
                success: function (data)
                {
                   alert(\"Bienvenido \"+data);
                }
            });
            return false;
            });


        \$(\"#btn_registro\").click(function () {
            \$.get(\"";
        // line 103
        echo $this->env->getExtension('routing')->getPath("registro");
        echo "\", function (htmlexterno) {
                \$(\"#index\").html(htmlexterno);
            });
        });
    </script>
</html>
";
    }

    public function getTemplateName()
    {
        return "AdminloginBundle:Default:formjavascript.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 103,  113 => 87,  52 => 29,  48 => 28,  25 => 8,  19 => 4,);
    }
}
