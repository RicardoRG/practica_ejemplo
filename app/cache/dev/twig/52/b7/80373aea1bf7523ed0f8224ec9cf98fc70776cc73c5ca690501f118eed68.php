<?php

/* AdminloginBundle:Default:index.html.twig */
class __TwigTemplate_52b780373aea1bf7523ed0f8224ec9cf98fc70776cc73c5ca690501f118eed68 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
    <body>
        <form method=\"post\"  align=\"middle\" id=\"frm_login\" >
            <p> <input type=\"text\" id=\"usr\" name=\"Email\"  placeholder=\"Usuario o Email\"></p>
            <p><input type=\"password\" id=\"password\" name=\"Password\" placeholder=\"Contraseña\"></p>
            <p>
                <a href=\"";
        // line 7
        echo $this->env->getExtension('routing')->getUrl("registro");
        echo "\">
                    Nuevo Registro
                </a>
            <p>
                <input id=\"btn_login\" type=\"submit\" name=\"commit\" value=\"Login\">
        </form>
    </body>
    <script src=\"";
        // line 14
        echo twig_escape_filter($this->env, $this->env->getExtension('assets')->getAssetUrl("public/js/jquery-1.11.3.min.js"), "html", null, true);
        echo "\"></script>
   <script type=\"text/javascript\">
    \$('#btn_login').click(function () {
        var data = \$(\"#frm_login\").serialize();
        var URL = \"";
        // line 18
        echo $this->env->getExtension('routing')->getPath("consulta");
        echo "\";
        console.log(data);
        \$.ajax({
            type: \"POST\",
            url: URL,
            data: {data},
            success: function (data)
            {
                \$(\"#resultado\").html(data);
            }
        });
        return false;
    });
</script>
</html>";
    }

    public function getTemplateName()
    {
        return "AdminloginBundle:Default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 18,  37 => 14,  27 => 7,  19 => 1,);
    }
}
