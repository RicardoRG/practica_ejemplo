<?php

namespace Admin\loginBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Admin\loginBundle\Model\ConsultasModel;

class DefaultController extends Controller {

    public function loginAction() {
        return $this->render('AdminloginBundle:Default:formjavascript.html.twig');
    }

    public function registroAction() {
        return $this->render('AdminloginBundle:Default:registrojs.html.twig');
    }

    public function consultaAction() {
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $post = $request->request->all();
        }
        parse_str($post["data"], $datos);
        $m = new ConsultasModel();
        $r = $m->Login($datos);
        print_r($r["data"][0]["Nombre"]);
        die();
    }

    public function registrarAction() {
        $request = $this->getRequest();
        if ($request->getMethod() == 'POST') {
            $post = $request->request->all();
        }
        parse_str($post["data"], $datos);
        $m = new ConsultasModel();
        $m->Registrar($datos);
    }

}
