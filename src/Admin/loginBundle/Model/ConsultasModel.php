<?php

namespace Admin\loginBundle\Model;

use Utilerias\FileMakerBundle\API\ODBC\Client;

class ConsultasModel {

    function __construct() {
        $this->FMClient = new Client();
    }

    public function Login($datos) {
        $query = 'SELECT ';
        $query .= 'Email, ';
        $query .='Password,';
        $query .= 'Nombre ';
        $query .=' FROM';
        $query .=' VIS ';
        $query .=' WHERE';
        $query .=' Email=' . "'" . $datos['Email'] . "'";
        $query .=' AND Password =' . "'" . $datos['Password'] . "'";
        $this->FMClient->setQuery($query);
        $this->FMClient->exec();
        $result = $this->FMClient->getResultAssoc();
        return $result;
    }

    public function Registrar($datos) {
        $query = 'INSERT ';
        $query .= 'INTO ';
        $query .=' VIS';
        $query .='(Nombre,';
        $query .='ApellidoPaterno,';
        $query .='ApellidoMaterno,';
        $query .='Email,';
        $query .='Password,';
        $query .='DE_Cargo,';
        $query .='DE_Razon_Social) ';
        $query .='VALUES(';
        $query .="'" . $datos['Nombre'] . "',";
        $query .="'" . $datos['ApellidoPaterno'] . "',";
        $query .="'" . $datos['ApellidoMaterno'] . "',";
        $query .="'" . $datos['Email'] . "',";
        $query .="'" . $datos['Password'] . "',";
        $query .="'" . $datos['DE_Cargo'] . "',";
        $query .="'" . $datos['DE_Razon_Social'] . "')";
        $this->FMClient->setQuery($query);
        $this->FMClient->exec();
        $result = $this->FMClient->getResultAssoc();
        
        return $result;
    }

}
